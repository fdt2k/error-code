'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

class ErrorWithCode extends Error {
  constructor(message, code) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.code = code;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor.name);
    }
  }

}

exports.ErrorWithCode = ErrorWithCode;
exports.default = ErrorWithCode;
